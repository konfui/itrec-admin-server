var firebase = require('firebase');
var firestore = require('firebase/firestore');

let config = {
    apiKey: 'AIzaSyCY9qu5vndXNQhH_BJPWXYhJqAd1mvcrp8',
    projectId: 'i-trec'
};

let app = firebase.initializeApp(config);

module.exports = {
    firestore: app.firestore()
};
