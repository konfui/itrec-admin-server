var port = process.env.PORT || 80
var pollingRate = process.env.POLLING_RATE || 500
var webSocketServer = require('websocket').server
var http = require('http')

var firebase = require('firebase')
var firestore = require('./Firebase.js').firestore

var state = {
    chatrooms: {}
}

var wsServer = undefined

var collectionRef = firestore.collection('chatroom')


notifyClients = (data) => {
    wsServer.broadcastUTF(JSON.stringify(data))
}

populateChatroom = async () => {
    return new Promise((resolve) => {
        // creating chatroom listener
        collectionRef.limit(10000).onSnapshot((snapshot) => {
            snapshot.docChanges().forEach(function (change) {
                if (change.type === 'added') {
                    notifyClients({ type: "new-room", id: change.doc.id, data: change.doc.data({ serverTimestamps: 'estimate' }) })
                    state.chatrooms[change.doc.id] = ({ data: change.doc.data({ serverTimestamps: 'estimate' }), chats: [] })
                    populateChats(change.doc.id)
                } else if (change.type === 'modified') {
                    notifyClients({ type: "rename-room", id: change.doc.id, name: change.doc.data()['name'] })
                    state.chatrooms[change.doc.id].data['name'] = change.doc.data()['name']
                }
            })
            resolve()
        })
    })
}

populateChats = async (id) => {
    return new Promise((resolve) => {
        // creating chat listener
        collectionRef.doc(id).collection('chats').orderBy('timestamp').onSnapshot((snapshot) => {
            if (state.chatrooms[id].chats.length === 0) {
                state.chatrooms[id].chats = {}
                snapshot.forEach(function (doc) {
                    state.chatrooms[id].chats[doc.id] = { id: doc.id, data: doc.data({ serverTimestamps: 'estimate' }) }
                })
            } else {
                snapshot.docChanges().forEach(function (change) {
                    if ((change.type === 'modified' && change.doc.data()['from_admin']) || (change.type === 'added' && !change.doc.data()['from_admin'] && !change.doc.data()['read'])) {
                        notifyClients({ type: "recieve-chat", chatroom: id, id: change.doc.id, data: change.doc.data({ serverTimestamps: 'estimate' }) })
                        state.chatrooms[id].chats[change.doc.id] = { id: change.doc.id, data: change.doc.data({ serverTimestamps: 'estimate' }) }
                    }
                })
            }
            resolve()
        })
    })
}

handleChatSubmit = async (chat, activeRoom) => {
    return new Promise((resolve) => {
        collectionRef.doc(activeRoom).collection('chats').add({
            content: chat,
            from_admin: true,
            read: true,
            timestamp: firebase.firestore.FieldValue.serverTimestamp()
        })
        resolve()
    })
}

clearBadge = async (name) => {
    return new Promise((resolve) => {
        let batch = firestore.batch()
        for (let id in state.chatrooms[name].chats) {
            batch.update(collectionRef.doc(name).collection('chats').doc(id), { read: true })
        }
        batch.commit()
        resolve()
    })
}

renameRoom = async (oldName, newName) => {
    return new Promise((resolve) => {
        collectionRef.doc(oldName).update({
            name: newName.trim(),
        })
        resolve()
    })
}

// populate
populateChatroom().then(() => {
    console.log((new Date()) + " Done populating chatroom!")
})

// ---------- init http server ----------
var server = http.createServer((request, response) => { })
server.listen(port, () => {
    console.log((new Date()) + " Server is listening on port " + port)
})

// init websocket server
wsServer = new webSocketServer({ httpServer: server })
wsServer.on('request', function (request) {
    var connection = request.accept(null, request.origin)
    console.log((new Date()) + ' Connection from ' + connection.socket.remoteAddress + ' accepted. Active clients: ' + wsServer.connections.length)

    if (state.chatrooms !== {}) {
        connection.sendUTF(JSON.stringify({ type: 'history', data: state.chatrooms }))
    }

    // client sent message
    connection.on('message', function (message) {
        if (message.type === 'utf8') {
            var json
            try {
                json = JSON.parse(message.utf8Data)
            } catch (e) {
                console.log('Invalid JSON: ', message.utf8Data)
                return
            }

            // parse message
            if (json.type === 'clear-badge') {
                console.log((new Date()) + " Clearing badge: " + json.chatroom)
                clearBadge(json.chatroom).then(notifyClients({ type: "clear-badge", chatroom: json.chatroom }))
            } else if (json.type === 'send-chat') {
                console.log((new Date()) + " Send chat: " + json.data + " @ " + json.chatroom)
                handleChatSubmit(json.data, json.chatroom)
            } else if (json.type === 'rename-room') {
                console.log((new Date()) + " Rename room: " + json.old + "->" + json.new)
                renameRoom(json.old, json.new)
            } else if (json.type === 'ping') {
                connection.sendUTF(JSON.stringify({ type: "pong" }))
            } else {
                console.log((new Date()) + " Unkown command: " + json.type)
            }
        }
    })

    // client disconnected
    connection.on('close', function (connection) {
        console.log((new Date()) + " Peer " + connection.toString() + " disconnected. Active clients: " + wsServer.connections.length)
    })
})